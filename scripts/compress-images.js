const path = require('path');
const glob = require("glob");
const sharp = require('sharp');

const files = glob.sync('src/assets/**/*.jpg', {ignore: '**/*-compressed.jpg'})
const config = {
  width: 4096,
  height: 2048,
  quality:  75
}

files.forEach((file) => {
  const { dir, name, ext} = path.parse(file);
  const { width, height, quality } = config;
  const targetFilePath = `${dir}/${name}-compressed${ext}`

  sharp(file)
    .resize(width, height)
    .jpeg({ mozjpeg: true, quality })
    .toFile(targetFilePath, (err, info) => {
      if (err) {
        throw new Error(`compress-images failed with file "${file}"`)
      } else {
        console.log("🗜 ", targetFilePath, Math.round(info.size/1024), "KB");
      }
    });
})