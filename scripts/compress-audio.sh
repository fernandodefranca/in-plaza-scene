# Recursive conversion of audio files
SrcDir="/Users/fernandodefranca/Dropbox/in-plaza/environments/"
find "$SrcDir" -type f -name '*.wav' -exec /bin/bash -c 'f2=$(basename "$1"); d2=$(dirname "$1"); ffmpeg  -i "$1" -c:a aac -vn -y "$d2/${f2%.*}.m4v" ' _ {}  \;