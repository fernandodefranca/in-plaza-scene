import { AxesHelper } from "three";

export function setupAxesHelper(scene) {
  const axesHelper = new AxesHelper(1);
  axesHelper.visible = false;
  scene.add(axesHelper);

  return axesHelper;
}
