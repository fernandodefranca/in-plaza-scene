export function listenScreenResize(screenSize, camera, renderers) {
  const listener = () => {
    // Update screensize
    screenSize.width = window.innerWidth;
    screenSize.height = window.innerHeight;

    // Update camera
    camera.aspect = screenSize.width / screenSize.height;
    camera.updateProjectionMatrix();

    // Update renderer
    renderers.forEach((renderer) => {
      renderer.setSize(screenSize.width, screenSize.height);
    });
  };

  window.addEventListener("resize", listener);
}
