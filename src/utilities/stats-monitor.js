import Stats from "three/examples/jsm/libs/stats.module.js";
import { config } from "../config";

export const stats = new Stats();
if (config.isDebug) document.body.appendChild(stats.dom);
