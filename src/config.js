export const config = {
  isProduction: process.env.NODE_ENV === "production",
  isDebug: document.location.hash === "#debug",
  fov: 70, // Vertical field of view
  initialSceneName: "initial",
};
