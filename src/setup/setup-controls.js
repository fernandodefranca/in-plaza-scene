import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";

export function setupControls(camera, canvas, autoRotate = false) {
  const controls = new OrbitControls(camera, canvas);
  controls.enableDamping = true;
  controls.dampingFactor = 0.15;
  controls.enablePan = false;
  controls.enableZoom = false;
  controls.rotateSpeed = -0.3;
  controls.autoRotate = autoRotate;

  return controls;
}
