import { CSS2DRenderer } from "three/examples/jsm/renderers/CSS2DRenderer.js";

export function setup2dRenderer(targetElementSelector, width, height) {
  const domRenderer = new CSS2DRenderer();
  domRenderer.setSize(width, height);
  domRenderer.domElement.style.position = "absolute";
  domRenderer.domElement.style.top = "0px";
  domRenderer.domElement.id = "dom-renderer";

  const targetElement = document.querySelector(targetElementSelector);
  targetElement.appendChild(domRenderer.domElement);

  return domRenderer;
}
