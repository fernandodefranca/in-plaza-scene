import { PerspectiveCamera } from "three";

export function setupCamera(fov, width, height) {
  const camera = new PerspectiveCamera(fov, width / height);
  camera.position.z = 0.1; // Hackish

  window.camera = camera;

  return camera;
}
