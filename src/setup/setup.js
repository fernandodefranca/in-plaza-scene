import { MeshBasicMaterial, Clock, Frustum, Matrix4, Scene } from "three";
import * as dat from "dat.gui";
import mitt from "mitt";
import { setupDomElements } from "./setup-dom-elements";
import { setupCamera } from "./setup-camera";
import { setupGeometry } from "./setup-geometry";
import { setupControls } from "./setup-controls";
import { setupRenderer } from "./setup-renderer";
import { setup2dRenderer } from "./setup-2d-renderer";
import { setupEnvironmentAudio } from "./setup-environment-audio";
import { setupSceneManager } from "./setup-scene-manager";
import { listenScreenResize } from "../utilities/listen-screen-resize";
import { setupAxesHelper } from "../utilities/setup-axes-helper";
import { stats } from "../utilities/stats-monitor";
import { config } from "../config";

const eventEmitter = mitt();
const clock = new Clock(); // Instantiate the clock
const sharedData = {
  screenSize: { width: window.innerWidth, height: window.innerHeight },
};

function setupScene(targetElementSelector) {
  const { screenSize } = sharedData;

  // Setup
  const { canvas } = setupDomElements(targetElementSelector);
  const renderer = setupRenderer(canvas, screenSize.width, screenSize.height);
  const domRenderer = setup2dRenderer(
    targetElementSelector,
    screenSize.width,
    screenSize.height
  );
  const camera = setupCamera(config.fov, screenSize.width, screenSize.height);
  const controls = setupControls(camera, canvas);
  const material = new MeshBasicMaterial({ color: 0x000000 });
  const mesh = setupGeometry(material);
  const environmentAudio = setupEnvironmentAudio(camera);
  const frustum = new Frustum();
  const cameraViewProjectionMatrix = new Matrix4();

  // Add elements to scene
  const scene = new Scene();
  scene.add(mesh);
  scene.add(camera);

  const render = () => {
    renderer.render(scene, camera);
    domRenderer.render(scene, camera);
  };

  // Handle resize
  listenScreenResize(screenSize, camera, [renderer, domRenderer]);

  // Update Frustrum so we can check if object are inside the viewport
  const updateFrustrum = () => {
    camera.updateMatrixWorld();
    camera.matrixWorldInverse.copy(camera.matrixWorld).invert();
    cameraViewProjectionMatrix.multiplyMatrices(
      camera.projectionMatrix,
      camera.matrixWorldInverse
    );
    frustum.setFromProjectionMatrix(cameraViewProjectionMatrix);
  };

  // Frustrum should be updated before checking
  const isObjectVisible = (obj) => frustum.intersectsObject(obj);

  return {
    render,
    screenSize,
    scene,
    camera,
    canvas,
    controls,
    mesh,
    environmentAudio,
    updateFrustrum,
    isObjectVisible,
  };
}

function startRenderLoop(entities) {
  const { render, controls, sceneManager } = entities;

  const tick = () => {
    config.isDebug && stats.begin(); // Start measuring performance
    controls.update(); // Otherwise damping stops with mouse movement

    // animate something
    // const elapsedTime = clock.getElapsedTime(); // Get elapsed seconds
    const hasLoop =
      sceneManager &&
      sceneManager.currentScene &&
      sceneManager.currentScene.loop;
    if (hasLoop) sceneManager.currentScene.loop({ ...entities, clock });

    render();

    config.isDebug && stats.end(); // End measuring performance
    window.requestAnimationFrame(tick); // Call tick again on the next frame
  };

  tick();

  return entities;
}

function addDebugInstruments(entities) {
  if (!config.isDebug) return entities; // Bypass if not debug mode

  const { mesh, sceneManager } = entities;
  const axesHelper = setupAxesHelper(entities.scene);

  const debugUI = {
    goToCinemateca: () => {
      sceneManager.replaceScene("initial");
    },
    toToRoosevelt: () => {
      sceneManager.replaceScene("roosevelt");
    },
  };

  const gui = new dat.GUI();
  gui.add(mesh.rotation, "y", -3, 3, 0.01).name("meshRotation");
  gui.add(axesHelper, "visible").name("display axis");
  // gui.add(debugUI, "toToRoosevelt").name("roosevelt");
  // gui.add(debugUI, "goToCinemateca").name("cinemateca");

  return entities;
}

export function setup(targetElementSelector, sceneConfig) {
  return Promise.resolve()
    .then(() => setupScene(targetElementSelector))
    .then((entities) => setupSceneManager({ ...entities, sceneConfig, eventEmitter }))
    .then(startRenderLoop)
    .then(({sceneManager}) => sceneManager.replaceScene(config.initialSceneName))
    .then(addDebugInstruments)
}