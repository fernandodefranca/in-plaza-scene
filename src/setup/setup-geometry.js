import { SphereGeometry, Mesh } from "three";

export function setupGeometry(material) {
  const geometry = new SphereGeometry(10, 60, 40); // TODO: Review hardcoded values
  geometry.scale(-1, 1, 1);

  return new Mesh(geometry, material);
}
