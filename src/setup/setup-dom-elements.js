export function setupDomElements(targetElementSelector) {
  const targetElement = document.querySelector(targetElementSelector);
  const canvas = document.createElement("canvas");
  canvas.style.position = "fixed";
  canvas.style.top = "0";
  canvas.style.left = "0";
  canvas.style.outline = "none";
  targetElement.appendChild(canvas);

  return { canvas, targetElement };
}
