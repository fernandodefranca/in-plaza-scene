import { fadeOutMesh } from "../utilities/fade-out-mesh";

function replaceTexture(entities, nextMaterial) {
  const { mesh } = entities;

  nextMaterial.opacity = 0;
  mesh.material = nextMaterial;

  return entities;
}

export function setupSceneManager(entities) {
  const { sceneConfig, eventEmitter } = entities;

  const sceneManager = {
    currentScene: null,
    isTransitioning: false,
    replaceScene: (newSceneName) => {
      if (sceneManager.isTransitioning) return entities;

      const newScene = sceneConfig.scenes[newSceneName];

      // Block if is the same scene
      if (newScene === sceneManager.currentScene) return entities;

      // Check if scene exists
      if (!newScene) throw new Error(`Invalid scene "${newSceneName}"`);

      return Promise.resolve()
        .then(() => {
          const { currentScene } = sceneManager;
          sceneManager.isTransitioning = true; // Block transition over transition
          eventEmitter.emit("SCENE-TRANSITION-STARTED");

          // Avoid slow fade out on first scene - there's nothing to fade
          const isFirstScene = currentScene === null;
          if (isFirstScene) return fadeOutMesh(entities, 0);

          return currentScene.onBeforeEnd(entities);
        })
        .then((entities) => {
          // Handle "onEnd" if exists
          const { environmentAudio } = entities;
          const { currentScene } = sceneManager;
          const onEnd = currentScene && currentScene.onEnd;

          // Stop previous scene audio
          if (environmentAudio.isPlaying) environmentAudio.stop();

          // Run onEnd
          if (!onEnd) return entities;

          return onEnd(entities);
        })
        .then((entities) => {
          // Handle scene preloading
          const shouldPreload = newScene.preload && !newScene.finishedPreload;

          if (shouldPreload) {
            eventEmitter.emit("PRELOAD-STARTED");
            return new Promise((resolve) => {
              newScene.preload().then(() => {
                eventEmitter.emit("PRELOAD-DONE");
                return resolve(entities);
              });
            });
          }

          return entities;
        })
        .then((entities) => {
          // Handle audio preload
          const hasAudio = !!newScene.preloadAudio;
          const shouldPreloadAudio = hasAudio && !newScene.finishedAudioPreload;
          const preloadFn = shouldPreloadAudio
            ? newScene.preloadAudio
            : () => Promise.resolve();
          const { environmentAudio } = entities;

          return new Promise((resolve) => {
            preloadFn()
              .then(() => {
                // Play audio, if has any
                if (hasAudio) {
                  environmentAudio
                    .setBuffer(newScene.audioBuffer)
                    .setLoop(true)
                    .play();
                }
              })
              .then(() => resolve(entities));
          });
        })
        .then((entities) => replaceTexture(entities, newScene.getMaterial()))
        .then((entities) => {
          // Update currentScene ref
          sceneManager.currentScene = newScene;

          return { ...entities, sceneManager };
        })
        .then((entities) => {
          // Handle "onBeforeStart" if exists
          const { currentScene } = sceneManager;

          if (!currentScene.onBeforeStart) return entities;

          return currentScene.onBeforeStart(entities);
        })
        .then(newScene.onStart)
        .then((entities) => {
          sceneManager.isTransitioning = false; // Block transition over transition
          eventEmitter.emit("SCENE-TRANSITION-DONE");
          return entities;
        });
    },
  };

  return { ...entities, sceneManager };
}
