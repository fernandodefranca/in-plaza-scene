import { sceneCinemateca } from "./scenes/cinemateca";
import { sceneBixiga } from "./scenes/bixiga";
import { sceneAnhangabau } from "./scenes/anhangabau";
import { sceneBelaVistaRoosevelt } from "./scenes/bela-vista-roosevelt";
import { sceneBras } from "./scenes/bras";
import { sceneCamposEliseos } from "./scenes/campos-eliseos";
import { sceneSe } from "./scenes/se";

import "./style.css";
import { setup } from "./setup/setup";

const sceneConfig = {
  scenes: {
    initial: sceneCinemateca,
    cinemateca: sceneCinemateca,
    bixiga: sceneBixiga,
    anhangabau: sceneAnhangabau,
    belaVistaRoosevelt: sceneBelaVistaRoosevelt,
    bras: sceneBras,
    camposEliseos: sceneCamposEliseos,
    se: sceneSe,
  },
};

setup("#scene-target", sceneConfig)
  .then(({ eventEmitter, sceneManager }) => {
    eventEmitter.on("HOTSPOT-CLICKED", (what) => {
      sceneManager.replaceScene(what);
    });
  });
