import { SceneModel } from "../models/scene-model";
import image from "../assets/anhangabau/texture-compressed.jpg";
import audio from "../assets/anhangabau/audio-loop.m4a";

export const sceneAnhangabau = new SceneModel()
  .withImagePreload([image])
  .withAudioPreload(audio)
  .withMaterialFn(() => sceneAnhangabau.materials[image])
  .withHotspot("cinemateca", 1, 0, -2, '', 'X');