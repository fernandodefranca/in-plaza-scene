import { SceneModel } from "../models/scene-model";
import image from "../assets/bela-vista-roosevelt/texture-compressed.jpg";
import audio from "../assets/bela-vista-roosevelt/audio-loop.m4a";

export const sceneBelaVistaRoosevelt = new SceneModel()
  .withImagePreload([image])
  .withAudioPreload(audio)
  .withMaterialFn(() => sceneBelaVistaRoosevelt.materials[image])
  .withHotspot("cinemateca", 1, 0, -2, '', 'X');