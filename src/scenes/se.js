import { SceneModel } from "../models/scene-model";
import image from "../assets/se/texture-compressed.jpg";

export const sceneSe = new SceneModel()
  .withImagePreload([image])
  .withMaterialFn(() => sceneSe.materials[image])
  .withHotspot("cinemateca", 1, 0, -2, '', 'X');