import { SceneModel } from "../models/scene-model";
import image from "../assets/campos-eliseos/texture-compressed.jpg";
import audio from "../assets/campos-eliseos/audio-loop.mp3";

export const sceneCamposEliseos = new SceneModel()
  .withImagePreload([image])
  .withAudioPreload(audio)
  .withMaterialFn(() => sceneCamposEliseos.materials[image])
  .withHotspot("cinemateca", 1, 0, -2, '', 'X');