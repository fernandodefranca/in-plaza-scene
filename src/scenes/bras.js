import { SceneModel } from "../models/scene-model";
import image from "../assets/bras/texture-compressed.jpg";

export const sceneBras = new SceneModel()
  .withImagePreload([image])
  .withMaterialFn(() => sceneBras.materials[image])
  .withHotspot("cinemateca", 1, 0, -2, '', 'X');