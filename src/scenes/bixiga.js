import { SceneModel } from "../models/scene-model";
import image from "../assets/bixiga/texture-compressed.jpg";
import audio from "../assets/bixiga/audio-loop.m4a";

export const sceneBixiga = new SceneModel()
  .withImagePreload([image])
  .withAudioPreload(audio)
  .withMaterialFn(() => sceneBixiga.materials[image])
  .withHotspot("cinemateca", 1, 0, -2, '', 'X');