import { SceneModel } from "../models/scene-model";
import image from "../assets/cinemateca/texture-compressed.jpg";
import audio from "../assets/cinemateca/audio-loop.m4v";
import { makeCirclePoints } from "../utilities/make-circle-points";

const points = makeCirclePoints(0, 0, 9, 6, 30)


const doors = { 
  'I': [points[2].x, 0, points[2].y], 
  'II': [points[3].x, 0, points[3].y], 
  'III': [points[4].x, 0, points[4].y], 
  'IV': [points[5].x, 0, points[5].y], 
  'V': [points[0].x, 0, points[0].y], 
  'VI': [points[1].x, 0, points[1].y],
}

export const sceneCinemateca = new SceneModel()
  .withImagePreload([image])
  .withAudioPreload(audio)
  .withMaterialFn(() => sceneCinemateca.materials[image])
  .withHotspot("se", ...doors['I'], null, "Sé")
  .withHotspot("anhangabau", ...doors['II'], null, "Anhangabaú")
  .withHotspot("belaVistaRoosevelt", ...doors['III'], null, "Bela-Vista Roosevelt")
  .withHotspot("bras", ...doors['IV'], null, "Brás")
  .withHotspot("camposEliseos", ...doors['V'], null, "Campos Elíseos")
  .withHotspot("bixiga", ...doors['VI'], null, "Créditos")
  .withLoop((entities) => {
    const elapsedTime = entities.clock.getElapsedTime();
    entities.mesh.rotation.x = Math.sin(elapsedTime) * 0.001;
    entities.mesh.rotation.z = Math.sin(elapsedTime) * -0.001;
  });
