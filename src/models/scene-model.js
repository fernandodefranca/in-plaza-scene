import {
  AudioLoader,
  LoadingManager,
  TextureLoader,
  MeshBasicMaterial,
  NearestFilter,
  SphereGeometry,
  Mesh,
} from "three";
import { CSS2DObject } from "three/examples/jsm/renderers/CSS2DRenderer.js";
import { fadeInMesh } from "../utilities/fade-in-mesh";
import { fadeOutMesh } from "../utilities/fade-out-mesh";
import { config } from "../config";

const noop = (entities) => entities;

const geometry = new SphereGeometry(0.1, 5, 5);
const material = new MeshBasicMaterial({ color: 0xff0000 });

export class SceneModel {
  constructor() {
    // functions
    this.getMaterial = noop;
    this.onBeforeStartFn = noop;
    this.onStart = fadeInMesh;
    this.onBeforeEndFn = null;
    this.onEnd = noop;
    this.loop = noop;
    this.preload = null; // Should be a Promise returning function
    this.preloadAudio = null; // Should be a Promise returning function

    // variables
    this.materials = {};
    this.finishedPreload = false;
    this.finishedAudioPreload = false;
    this.loadingManager = null;
    this.audioBuffer = null;
    this.hotspots = [];
    this.hotspotsObjs = [];
    this.hotspotsElements = [];

    this.onBeforeStart = (entities) => {
      // Handle control/camera object tracking
      const { scene, eventEmitter } = entities;
      const { hotspots, hotspotsObjs, hotspotsElements } = this;

      if (hotspots.length) {
        // If necessary, create objects and DOM element for hotspots
        if (!hotspotsObjs.length) {
          hotspots.forEach(({ name, x, y, z, className, content }) => {
            // Create geometry
            const obj = new Mesh(geometry, material);
            obj.position.x = x;
            obj.position.y = y;
            obj.position.z = z;
            obj.name = name;
            obj.visible = !!config.isDebug;

            scene.add(obj); // Add to scene
            hotspotsObjs.push(obj);

            // Create hotspots DOM elements
            const div = document.createElement("div");
            div.setAttribute('data-hotspot-name', name);
            div.className = `hotspot ${className || ''}`;
            if (content) div.innerHTML = content;
            hotspotsElements.push(div);

            const css2dObj = new CSS2DObject(div);
            css2dObj.position.set(0, 0, 0);
            obj.add(css2dObj);

            div.addEventListener("click", (evt) => {
              const name = evt?.target?.getAttribute('data-hotspot-name');
              if (name) eventEmitter.emit("HOTSPOT-CLICKED", name);
            });

            return obj;
          });
        }

        // Ensure to re-enable hotspots visibilty
        hotspotsObjs.forEach((obj) => scene.add(obj));
        hotspotsElements.forEach((el) => (el.style.display = "block"));
      }

      if (this.onBeforeStartFn) {
        return this.onBeforeStartFn(entities);
      }

      return entities;
    };

    this.onBeforeEnd = (entities) => {
      // Clean-up
      const { scene } = entities;

      // Hide hotspots
      this.hotspotsObjs.forEach((obj) => scene.remove(obj));
      this.hotspotsElements.forEach((el) => (el.style.display = "none"));

      if (this.onBeforeEndFn) {
        return this.onBeforeEndFn(entities).then(fadeOutMesh);
      }

      return fadeOutMesh(entities);
    };
  }

  withMaterialFn(getMaterialFn) {
    this.getMaterial = getMaterialFn;

    return this;
  }

  withOnBeforeStart(onBeforeStartFn) {
    this.onBeforeStartFn = onBeforeStartFn;

    return this;
  }

  withOnStart(onStartFn) {
    this.onStart = onStartFn;

    return this;
  }

  withOnBeforeEnd(onBeforeEndFn) {
    this.onBeforeEndFn = onBeforeEndFn;

    return this;
  }

  withOnEnd(onEndFn) {
    this.onEnd = onEndFn;

    return this;
  }

  withLoop(loopFn) {
    this.loop = loopFn;

    return this;
  }

  withImagePreload(imagesArr) {
    const { materials } = this;
    this.loadingManager = new LoadingManager();

    this.preload = () => {
      return new Promise((resolve, reject) => {
        this.loadingManager.onLoad = () => {
          console.log("loading finished");
          resolve();
        };
        this.loadingManager.onError = (err) => {
          console.log("loading error");
          reject(`loadTextureAndMaterials failed: "${err.message}"`);
        };

        const textureLoader = new TextureLoader(this.loadingManager);

        imagesArr.forEach((imgUrl) => {
          const texture = textureLoader.load(imgUrl);
          texture.minFilter = NearestFilter;
          texture.generateMipmaps = false;

          const material = new MeshBasicMaterial({ map: texture });
          material.transparent = true; // alows opacity

          materials[imgUrl] = material;
        });
      });
    };

    return this;
  }

  withAudioPreload(audioUrl) {
    if (!audioUrl) return this;

    this.preloadAudio = () => {
      return new Promise((resolve) => {
        const audioLoader = new AudioLoader();

        audioLoader.load(audioUrl, (loadedBuffer) => {
          this.audioBuffer = loadedBuffer;
          this.finishedAudioPreload = true;

          resolve();
        });
      });
    };
    return this;
  }

  withHotspot(name, x, y, z, className='', content='') {
    this.hotspots.push({ name, x, y, z, className, content });

    return this;
  }
}
